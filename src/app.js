const express = require("express")
const app = express();
var cors = require('cors')
app.use(express.json());
const fetch = require("node-fetch");
app.use(cors());

const port = process.env.PORT || 8080;


app.get("/data", async (req, res) => {
    const url = "http://45.79.111.106/interview.json"
    const resData = await fetch(url);
    const json = await resData.json();
    console.log(json)
    res.send({
        data: json
    })


})


app.listen(port, () => {
    console.log(`connection establised on ${port}`);
})